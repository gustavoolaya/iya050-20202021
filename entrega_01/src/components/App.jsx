import React from 'react';
import Register from './Register.jsx';
import Login from './Login.jsx';
import User from './User.jsx';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
  } from "react-router-dom";
import './style.css';
import { SnackbarProvider } from 'notistack';

function App (props){
    return (
    <SnackbarProvider maxSnack={3}>
       <Router>
            <div>
                <nav>
                    <ul>
                        <li>
                        <Link to="/">Home</Link>
                        </li>
                        <li>
                        <Link to="/login">Login</Link>
                        </li>
                        <li>
                        <Link to="/user">User</Link>
                        </li>
                        <li>
                        <Link to="/register">Registro</Link>
                        </li>
                    </ul>
                </nav>
                <Switch>
                    <Route exact path="/">
                        <Login />
                    </Route>
                    <Route path="/login">
                        <Login />
                    </Route>
                    <Route path="/register">
                        <Register />
                    </Route>
                    <Route path="/user">
                        <User />
                    </Route>
                </Switch>
            </div>
       </Router> 
    </SnackbarProvider>
    );
}

export default App;