import React, { useState } from 'react';
import { useHistory } from "react-router-dom";
import { useSnackbar } from 'notistack';

const Login = () => {
    let history = useHistory();
    const { enqueueSnackbar, closeSnackbar } = useSnackbar();

    const [user, setUser] = useState('');
    const [password, setPassword] = useState('');

    async function login(){
        const API = "https://6fra5t373m.execute-api.eu-west-1.amazonaws.com/development/";
        const dataUser = {
            "password": password
          }

    const settings = {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
        },
        body : JSON.stringify(dataUser)
    };
    try {
        const fetchResponse = await fetch(`${API}users/${user}/sessions`, settings);
        const data = await fetchResponse.json();
        console.log(data)
        const { sessionToken} = data;
        if (sessionToken !== undefined ){
            history.push("/user");
            enqueueSnackbar("Datos correctos", { 
                variant: 'success',
            });
        }
        return data;
    } catch (e) {
        return e;
    }    
    }

    function handleClick(){
        login();
        
    }

    return (
        <div className="container-form">
            <h1>Log-in</h1>
            <div> 
                <label>Usuario: </label>
                <input type="text" onChange={event => setUser(event.target.value)}/><br></br>
                <label>Contraseña: </label>
                <input type="password" onChange={event => setPassword(event.target.value)}/><br></br>
                <button type="buttom" form="form1" value="Submit" onClick={handleClick}>Entrar</button>
            </div>
        </div>
    );
}
export default Login;