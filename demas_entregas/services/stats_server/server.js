const { GraphQLServer } = require('graphql-yoga');
const fetch = require('node-fetch');
const { v4: uuidv4 } = require('uuid');

const typeDefs = `
input GameInput {
    name: String
}

input RoadInput{
    health: Int,
    score: Int,
    level: LevelInput
}

input LevelInput{
    current: Int,
    max: Int
}

type Road{
    id: ID!
    health: Int,
    score: Int,
    level: Level,
    time: String
}

type Level{
    current: Int,
    max: Int
}

type Response {
	status: String
}

type Root {
    id: ID
	createdAt: String
	value: String
	key: String!
	applicationId: String!
}

type Query {
	all: [Root!]!,
    root(key: String!): Root!,
    allGame:[Road],
    game(id: ID!):Road
}

type Mutation {
    newGame(input:RoadInput): Road
	createLogin(input: GameInput): Response
    updateLogin(id: ID!, input: GameInput): Response
    kill(id: ID!):Road,
    alive(id: ID!):Road,
    incrementScore(id: ID!):Road,
    decrementScore(id: ID!):Road,
}`;

const BASE_URL = 'https://0qh9zi3q9g.execute-api.eu-west-1.amazonaws.com/development/'

const PREFIX = 'race__'

const _headers = {
    'Content-Type': 'application/json',
    'x-application-id': 'gustavo.olaya'
};

const config_fetch_get = {
    method: 'GET',
    mode: 'cors',
    headers: _headers
};

const getGame = async (id) =>{

    let resourceUrl = `${BASE_URL}collections/${PREFIX}${id}`;

    const requestGameState = await fetch(resourceUrl, config_fetch_get);
    const responseGameState = await requestGameState.json();

    console.log({
        responseGameState
    });

    if ( responseGameState.length <= 0) return {};

    return JSON.parse(responseGameState[0].value);
}

const changeStateGame = async (id, key, increment) => {

    const currentGameState = await getGame(id);

    if (Object.keys(currentGameState).length === 0 && currentGameState.constructor === Object) return { status: "no data"}


    let newGameState = {
        ...currentGameState
    };


    console.log("before => ", {
        currentGameState
    });

    if (increment){
        newGameState[key]++;
    }else{
        newGameState[key]--;
    }

    console.log("after => ", {
        newGameState
    });
    

    const resourceUrl = `${BASE_URL}pairs/${PREFIX + id}`;

    

    const requestSaveData = await fetch(resourceUrl, {
        method: 'PUT',
        body: JSON.stringify(newGameState),
        headers: _headers
    });

    const responseSaveData = await requestSaveData.json();

    console.log({
        responseSaveData
    });

    return JSON.parse(responseSaveData.value);

}

const resolvers = {
    Query: {
        all: async() => {
            return fetch(`${BASE_URL}pairs`, config_fetch_get).then(res => res.json());
        },
        root: async(_, { key }) => {
            return fetch(`${BASE_URL}${key}`, config_fetch_get).then(res => res.json());
        },
        allGame: async() => {
           
            const _endpoint = `${BASE_URL}/collections/${PREFIX}`;

            const requestAllGame = await fetch(_endpoint, config_fetch_get);

            const allGameResponse = await requestAllGame.json();

            console.log(allGameResponse);

            return allGameResponse.map(item => JSON.parse(item.value));

        },
        game: async (_, { id }, context, info) => {

            const _endpoint = `${BASE_URL}/collections/${PREFIX}`;
           
            const requestGame = await fetch(_endpoint, config_fetch_get);
            const gameResponse = await requestGame.json();

           console.log(gameResponse);

           
            const allGameResponse = gameResponse
                .map(item => JSON.parse(item.value));

            const filteredGame = allGameResponse.filter(item => item.id === id);

            console.log(filteredGame);

            return filteredGame[0];
        }
    },
    
    Mutation: {
        newGame: async(root, args, context, info) => {
            console.log("Crear", {...args.input}); 

            const id = uuidv4();

            const _newGame = {
                id: id,
                ...args.input,
                time: new Date().getTime()
            }

            const resourceUrl = `${BASE_URL}pairs/${PREFIX}${id}`;

            const newGameRequest = await fetch(resourceUrl, {
                    method: 'PUT', // or 'PUT'
                    body: JSON.stringify(_newGame),
                    headers: _headers
                });

                const newGameResponse = await newGameRequest.json();

                console.log(newGameResponse);

               
            return JSON.parse(newGameResponse.value);
        },

        createLogin: async(root, args, context, info) => {
            //console.log(args);
            //console.log("Object => ", { ...args});
            console.log("Crear", {...args.input}); 
            
            const id = uuidv4();
            const loginState = {
                id: id,
                ...args.input,
                time: new Date().getTime()
            }

            const newStateGame = await fetch(BASE_URL + id, {
                    method: 'PUT', // or 'PUT'
                    body: JSON.stringify(loginState),
                    headers: _headers
                }).then(res => res.json())
                .catch(error => {
                    return {
                        "status": error
                    };
                })
                .then((response) => {
                    console.log("response => ", response);
                    const data = JSON.parse(response.value);
                    return {
                        "status": data.time
                    };
                });

            return newStateGame;
        },

        updateLogin: async(root, args, context, info) => {

            const updateLogin = await fetch(BASE_URL + args.id, {
                    method: 'PUT', // or 'PUT'
                    body: JSON.stringify(args.input),
                    headers: _headers
                }).then(res => res.json())
                .catch(error => {
                    return {
                        "status": "error",
                        "response": error
                    };
                })
                .then((response) => {

                    const login = {
                        status: "success",
                        response: JSON.parse(response.value)
                    }

                    return login;
                });

            return updateLogin;
        },

        kill: async(root, { id }, context, info) => {
            console.log("I'll kill => ", id); 
            
            return changeStateGame(id, 'health', false);

        },
        alive: async(root, { id }, context, info) => {
            console.log("I'll alive => ", id); 
            
            return changeStateGame(id, 'health', true);

        },
        incrementScore: async(root, { id }, context, info) => {
            console.log("I'll add score => ", id); 
            
            return changeStateGame(id, 'score', true);

        },
        decrementScore: async(root, { id }, context, info) => {
            console.log("I'll decrement score => ", id); 
            
            return changeStateGame(id, 'score', false);

        },
    }
};

const server = new GraphQLServer({ typeDefs, resolvers })

server.start({
    playground: '/playground',
    port: 3000 // process.env.PORT
}, () => {
})