import React from "react";
import { Link } from "react-router-dom";

const Landing = () => {
  return (
    <>
    <div className="home">
    <h1>Entrega 02</h1>
      <p>
        Try <Link to="/sign-in">logging in</Link>.
      </p>
    </div>
      
    </>
  );
};

export default Landing;
