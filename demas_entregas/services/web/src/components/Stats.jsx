import React, { useContext, useState, useEffect } from "react";
import AuthContext from "../AuthContext.js";

const Stats = () => {
  const { user } = useContext(AuthContext);
  const [allGame, setAllGame] = useState([]);

  function changeState(id, event){
    console.log("Play ", id);

    const requestChangeState = fetch( `http://localhost:6002/game/${id}/event`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
      body: JSON.stringify({event: event})
    }).then(r => r.json())
        .then(res =>{
          console.log('data returned:', res);
        } );


  }

  useEffect(()=>{
    const queryString=`
    query{
      allGame{
        id,
        health,
        score,
        level {
          current,
          max
        },
        time
      }
    }`;

    const requestAllGame = fetch('http://localhost:6006/', { //https://entrega-2.glitch.me/
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
      body: JSON.stringify({query: queryString})
    })
        .then(r => r.json())
        .then(res =>{
          console.log('data returned:', res);
          console.log('data returned:', res.data.allGame);
          setAllGame(res.data.allGame);
        } );
  },[]);

  const listAllGame = allGame.map((item) =>
      <li key={item.id}>{item.id } - {item.health } - {item.score } <button onClick={() => changeState(item.id, "kill")}>Kill</button> <button onClick={() => changeState(item.id, "alive")}>alive</button> <button onClick={() => changeState(item.id, 'incrementScore')}>Add score</button>  <button onClick={() => changeState(item.id, 'decrementScore')}>reduce score</button> </li>
  );

  return (
      <div>
        <p>{user.name}’s stats</p>

          <h1>Total Games: { listAllGame.length } </h1>
        <ul>{ listAllGame }</ul>
      </div>

  );



};

export default Stats;
