import React, { useContext, useEffect, useState } from "react";
import AuthContext from "../AuthContext.js";

const History = () => {
  const { user } = useContext(AuthContext);
  const [allGame, setAllGame] = useState([]);


  useEffect(()=>{
    const queryString=`
    query{
      allGame{
        id,
        health,
        score,
        level {
          current,
          max
        },
        time
      }
    }`;

    const requestAllame = fetch('http://localhost:6006/', { //https://entrega-2.glitch.me/
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
      body: JSON.stringify({query: queryString})
    })
        .then(r => r.json())
        .then(res =>{
          console.log('data returned:', res);
          console.log('data returned:', res.data.allGame);
          setAllGame(res.data.allGame);
        } );
  },[]);

  const listAllGame = allGame.map((item) =>
      <li key={item.id}>{item.id } - {item.health } - {item.score } </li>
  );

  return (
      <div>
        <p>{user.name}’s past games</p>

        <h1>Total Games: { listAllGame.length } </h1>
        <ul>{ listAllGame }</ul>
      </div>

  );


};

export default History;
