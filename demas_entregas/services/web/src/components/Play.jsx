import React, {useContext, useEffect, useState} from "react";
import AuthContext from "../AuthContext.js";

const Play = () => {

  const { isLoggedIn, user } = useContext(AuthContext);
  const [status, setStatus] = useState("");

  const [newGame, setNewGame] = useState({});

  function changeState(id, event){
    console.log("Play ", id);

    const requestChangeState = fetch( `http://localhost:6002/game/${id}/event`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
      body: JSON.stringify({event: event})
    }).then(r => r.json())
        .then(res =>{
          console.log('data returned:', res);
          if (Object.keys(res).length > 0 ){
            setNewGame(res);
          }

        } );


  }

  useEffect(()=>{

    const requestChangeState = fetch( `http://localhost:6002/game`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
      body: JSON.stringify({event: event})
    }).then(r => r.json())
        .then(res =>{
          console.log('data returned:', res);
          setNewGame(res);
        } );


    const queryString=`
    query{
      all{
        value
      }
    }`;

    //console.log("Esto eso ->",queryString.toString());

    fetch('http://localhost:6006/', { //https://entrega-2.glitch.me/
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
      body: JSON.stringify({query: queryString})
    })
      .then(r => r.json())
      .then(data => {
      	console.log('data returned:', data)
        console.log("esto es->", data)
      	setStatus(data.createLogin.status);
        
      });
  },[]);
    
      
  return ( 
    <div>
      <p>Name: {user.name}</p>
      <p>Date: {new Date(parseInt(user.date)).getDay()}-{new Date(parseInt(user.date)).getMonth()+1} - {new Date(parseInt(user.date)).getUTCFullYear()}</p>

      <h1>New game created</h1>

      <p>
        id: { newGame.id }
      </p>
      <p>
        health: { newGame.health }
      </p>
      <p>
        score: { newGame.score }
      </p>

      <div>
        <button onClick={() => changeState(newGame.id, "kill")}>Kill</button> <button onClick={() => changeState(newGame.id, "alive")}>alive</button> <button onClick={() => changeState(newGame.id, 'incrementScore')}>Add score</button>  <button onClick={() => changeState(newGame.id, 'decrementScore')}>reduce score</button>
      </div>
    </div>
  );
};

export default Play;