import React, { useContext, useEffect } from "react";
import { Link, Redirect, Route, Switch, useRouteMatch } from "react-router-dom";
import AuthContext from "../AuthContext.js";
import Stats from "./Stats.jsx";
import Play from "./Play.jsx";
import History from "./History.jsx";

const Menu = () => {
  const { isLoggedIn, user, logIn } = useContext(AuthContext);
  const { path, url } = useRouteMatch();

  if (!isLoggedIn) return <Redirect to="/sign-in" />;

  useEffect(()=>{
    const queryString=`
    mutation{
      createLogin(input:{
        name: "${user.name}"
      }){
        status
      }
    }`;

    fetch('http://localhost:6006/', { //https://entrega-2.glitch.me/
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
      },
      body: JSON.stringify({query: queryString})
    })
      .then(r => r.json())
      .then(res =>{
        console.log('data returned:', res);
        console.log('data returned:', res.data.createLogin.status);
        logIn({
          ...user,
          date: res.data.createLogin.status,
        });
      } );
  },[]);

  return (
    <>
      <nav>
        <ul>
          <li>
            <Link to={`${url}/stats`}>Stats</Link>
          </li>
          <li>
            <Link to={`${url}/play`}>New game</Link>
          </li>
          <li>
            <Link to={`${url}/history`}>Past games</Link>
          </li>
        </ul>
      </nav>
      <Switch>
        <Route exact path="/">
          <h2>Welcome, {user.name}</h2>
        </Route>

        <Route path={`${path}/stats`}>
          <Stats />
        </Route>

        <Route path={`${path}/play`}>
          <Play />
        </Route>

        <Route path={`${path}/history`}>
          <History />
        </Route>
      </Switch>
    </>
  );
};

export default Menu;
