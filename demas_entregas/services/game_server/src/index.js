const express = require('express')
const bodyParser = require('body-parser')
const app = express()
const port = 4000
let score = 0;

app.use(bodyParser.json())

app.get('/', (req, res) => {
  res.send('Welcome to game server service v1.0')
})

app.get('/newGame', (req, res) => {
  return res.json(newGame())
})

function newGame() {
  return {
    health: 3,
    score: 0,
    level: {
      current: 1,
      max: 3
    }
  }
}

// /mover_carrito
// movimientos = {
//   posicion: 100,
//   left: true,
//   right: false
// }

app.post('/mover_carrito', (req, res) => {
  const { movimientos } = req.body;
  let newPosition = {...movimientos};

  if (newPosition.left) {
    newPosition.posicionX = newPosition.posicionX + 5;
  }
  if (newPosition.right) {
    newPosition.posicionX = newPosition.posicionX - 5;
  }
  if (newPosition.up) {
    newPosition.posicionY = newPosition.posicionY + 5;
  }
  if (newPosition.down) {
    newPosition.posicionY = newPosition.posicionY - 5;
  }
  res.json(newPosition)
})

// /score
app.get('/score', (req, res) => {
  const { _new } = req.query;
  if(_new === 'true') {
    score = 0;
  } else {
    score++;
  }
  res.json({ score })
})

// /enemies
app.post('/enemies', (req, res) => {
  const { enemies } = req.body;
  enemies.push(Math.floor(Math.random() * (10 - 1)));

  console.log(enemies.length);
  if(enemies.length > 5){
    enemies.splice(0,1)
  }
  res.json({enemies})
})

app.listen(port, () => {
  console.log(`Servidor corriendo en http://localhost:${port}`)
})