const Koa = require("koa");
const cors = require("@koa/cors");
const koaBody = require("koa-body");
const Router = require("@koa/router");
const fetch = require('node-fetch');

const app = new Koa();
const router = new Router();

const GAME_SERVER = "http://gustavo0olayagame_server:4000"
const STATS_SERVER = "http://gustavo0olayastats_server:3000"

let configurationPostRequest = {
  method: 'post',
  headers:{
    "Content-Type":"application/json"
  },
  body: JSON.stringify({}),
}

const configurationGetRequest = {
  method: 'get',
  headers:{
    "Content-Type":"application/json"
  }
};

router.post("/game", async (ctx) => {
  // 1. post mutation to Stats server to store new game
  
  // 2. return state

  const requestNewGame = await fetch(`${GAME_SERVER}/newGame`, configurationGetRequest);
  const responseNewGame = await requestNewGame.json();

  console.log({
    responseNewGame
  });


   //Save new game in server stats

   const stringQuery = `
   mutation{
     newGame( input:{
      health: ${ responseNewGame.health || 0 },
      score: ${ responseNewGame.score || 0 },
      level: { 
        current: ${responseNewGame.level.current || {} }, 
        max:  ${responseNewGame.level.max || {} }, 
        },
     }){
       id
       health
       score
       level {
        current
        max
       }
       time
     }
   }
`;

configurationPostRequest.body = JSON.stringify({ query:  stringQuery});


console.log({
  configurationPostRequest
});

const requestSaveNewGame = await fetch(STATS_SERVER,configurationPostRequest);
const responseSaveNewGame = await requestSaveNewGame.json();


console.log({responseSaveNewGame});

const { data } = responseSaveNewGame;
  const { newGame } = data;

  console.log({
    newGame
  })

  ctx.response.set("Content-Type", "application/json");
  ctx.body = JSON.stringify({
    ...newGame
  });

});

router.get("/game/:id", async (ctx) => {
  // 1. get current state from Stats server
  // 2. return state

  const idGame = ctx.params.id;

  const stringQuery = `
  query{
    game(id:"${ idGame }"){
      id,
      health,
      score,
      level {
        current,
        max
      },
      time
      }
    }`;


    configurationPostRequest.body = JSON.stringify({ query:  stringQuery});


    console.log({
      configurationPostRequest
    });
    
    const requestGame = await fetch(STATS_SERVER,configurationPostRequest);
    const responseGame = await requestGame.json();

  console.log({
    responseGame
  });


  const { data } = responseGame;
  const { game } = data;

  console.log({
    game
  })

  ctx.response.set("Content-Type", "application/json");
  ctx.body = JSON.stringify({
    ...game
    // ...
  });
});

router.post("/game/:id/event", async (ctx) => {
  // 1. get current state from Stats server
  // 2. get next state from Game server
  // 3. post mutation to Stats server to store next state
  // 4. return state


  /*
  values for eventGame
    * kill
    * alive
    * decrementScore
    * incrementScore
  */

  let newStateGame = {};

  const eventGame = ctx.request.body.event || '';
  const idGame = ctx.params.id;


    const stringQuery = `
    mutation{
    ${ eventGame }(id:"${ idGame }"){
      id,
      health,
      score,
      level {
        current,
        max
      },
      time
      }
    }`;  


    configurationPostRequest.body = JSON.stringify({ query:  stringQuery});


    console.log({
      configurationPostRequest
    });
    
    const requestGame = await fetch(STATS_SERVER,configurationPostRequest);
    const responseGame = await requestGame.json();

  console.log({
    responseGame
  });

  console.log(
    responseGame.errors
);


  const { data } = responseGame;
  newStateGame  = data[eventGame];

  console.log({
    newStateGame
  })



  ctx.response.set("Content-Type", "application/json");
  ctx.body = JSON.stringify({
   ...newStateGame
  });
});

app.use(koaBody());
app.use(cors());
app.use(router.routes());
app.use(router.allowedMethods());

app.listen(process.env.PORT || 8080);
